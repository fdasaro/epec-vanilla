#script(python)
from probabilitysupport import *
#end.

isTrue(true).

% ----------------------------+
% EVALUATION OF PROBABILITIES |
% ----------------------------+
% Evaluation of p-propositions:
evalPProp(TR,A,I,P) :- trace(TR), action(A), instant(I), occurs(TR,A,I,P), holds(TR, ((A,true),I) ).
evalPProp(TR,A,I,@sub("1",P)) :- trace(TR), action(A), instant(I), occurs(TR,A,I,P), holds(TR, ((A,false),I) ).

% evaluation of Trace*Narrative:
evalTraceNarrative(TR,@log_prob_to_prob_string(P)) :-
   P = #sum{ @log_prob(X),-1,O : initialChoice(TR,(O,X));
             @log_prob(X),I,O  : effectChoice(TR,(O,X),I);
             @log_prob(X),I,O  : sensedValue(TR,(O,X),I);
             @log_prob(X),I,A  : evalPProp(TR,A,I,X) },
   trace(TR).

% ----------------------+
% FIXPOINT CONSTRUCTION |
% ----------------------+

:- trace(TR),
   action(A),
   instant(I),
   performed(A, I, P, Precondition, LeftBracket, X, Y, RightBracket),
   occurs(TR, A, I, P),
   B = #sum{ @prob(E),OTHER_TR : sensingIndistinguishableUpTo(TR,OTHER_TR,I-1), holds(OTHER_TR, (Precondition,I) ), evalTraceNarrative(OTHER_TR,E) },
   C = #sum{ @prob(F),OTHER_TR : sensingIndistinguishableUpTo(TR,OTHER_TR,I-1), evalTraceNarrative(OTHER_TR,F) },
   D = @div(@prob_to_string(B),@prob_to_string(C)),
   not isTrue(@in_between(@prob(D),LeftBracket,@prob(X),@prob(Y),RightBracket,"True")).

:- trace(TR),
   action(A),
   instant(I),
   performed(A, I, P, Precondition, LeftBracket, X, Y, RightBracket),
   not occurs(TR, A, I, P),
   B = #sum{ @prob(E),OTHER_TR : sensingIndistinguishableUpTo(TR,OTHER_TR,I-1), holds(OTHER_TR, (Precondition,I) ), evalTraceNarrative(OTHER_TR,E) },
   C = #sum{ @prob(F),OTHER_TR : sensingIndistinguishableUpTo(TR,OTHER_TR,I-1), evalTraceNarrative(OTHER_TR,F) },
   D = @div(@prob_to_string(B),@prob_to_string(C)),
   isTrue(@in_between(@prob(D),LeftBracket,@prob(X),@prob(Y),RightBracket,"False")).

#show evalTraceNarrative/2.
#show holds/2.
#show isActuallyPerformed/3.
#show sensedValue/3.
#show objectOf/2.
#show indistinguishableSprops/2.