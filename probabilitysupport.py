# Notice that maximum precision is 9
LOG_PRECISION = 9
PROB_PRECISION = 9

import clingo
import math

def log_prob_to_prob_string(n):
    n = math.exp(-n.number/math.pow(10,LOG_PRECISION-2))
    return clingo.String(str(n))

def prob_to_string(n):
    n = float(n.number/math.pow(10,PROB_PRECISION))
    return clingo.String(str(n))

def log_prob(n):
    n = float(n.string)
    if (n>0):
        log_n = math.log(n)
        return clingo.Number(int(round(-log_n*(math.pow(10,LOG_PRECISION-2)))))
    else:
        return 0

def prob(n):
    n = float(n.string)
    return clingo.Number(int(n*math.pow(10,PROB_PRECISION)))

def in_between(x,lb,a,b,rb,expected):
    """
    Checks whether a <= x <= b
    """
    #print("\tchecking whether %f is in %s%f,%f%s: Got %s (expected %s)" % (float(x.number),lb.string,float(a.number),float(b.number),rb.string,((float(x.number)>=float(a.number)) and (float(x.number)<=float(b.number))),expected.string))
    if ((float(x.number)>=float(a.number)) and (float(x.number)<=float(b.number))):
        return clingo.Function("true", [])
    else:
        return clingo.Function("false", [])

def sum(a,b): return clingo.String(str(float(a.string)+float(b.string)))
def mul(a,b): return clingo.String(str(float(a.string)*float(b.string)))
def sub(a,b): return clingo.String(str(float(a.string)-float(b.string)))
def div(a,b): return clingo.String(str(float(a.string)/float(b.string)))
