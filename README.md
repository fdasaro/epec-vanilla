# EPEC-ASP v0.1.0

The **Epistemic Probabilistic Event Calculus** (EPEC) is a probabilistic and epistemic extension of a popular framework for reasoning about the effects of a narrative of action occurrences (events) along a time line known as the Event Calculus (EC).

The formal syntax and semantics of EPEC was described in the paper: [*D'Asaro, F. A., Bikakis, A., Dickens, L., & Miller, R. (2020). Probabilistic reasoning about epistemic action narratives. Artificial Intelligence, 287, 103352*](http://doi.org/10.1016/j.artint.2020.103352).

This repository hosts a `clingo` implementation of EPEC that may be used in conjunction with the javascript interface available at [https://www.ucl.ac.uk/infostudies/epec/translator.html](https://www.ucl.ac.uk/infostudies/epec/translator.html).

## Prerequisites

In order to run `EPEC-ASP` you need to have a working Python 3 installation (see https://opensource.com/article/19/5/python-3-default-mac for a tutorial), the versioning system `git`, and a python-enabled `clingo` (version >= 5.5.0). If you use [Anaconda](https://conda.io/projects/conda/en/latest/user-guide/install/index.html) (or Miniconda) you can easily install a python-enabled `clingo` in a conda environment `potassco` by running

```bash
conda create -n potassco -c conda-forge clingo
conda activate potassco
```

or you can use `pip`:

```bash
python3 -m pip install --user --upgrade clingo
```

If you have linux and the apt package manager, you can install clingo with:

```bash
sudo apt install gringo clasp
```

To ensure your clingo installation has python support, run `clingo -v` and you should get output similar to the following:

```tex
clingo version 5.5.1
Address model: 64-bit

libclingo version 5.5.1
Configuration: with Python 3.10.0, with Lua 5.4.3

libclasp version 3.3.6 (libpotassco version 1.1.0)
Configuration: WITH_THREADS=1
Copyright (C) Benjamin Kaufmann

License: The MIT License <https://opensource.org/licenses/MIT>
```

## Installation

To install EPEC, first clone the repository running

```bash
git clone https://gitlab.com/fdasaro/epec-vanilla.git
```

First, you need to set the environmental variable `EPEC_INSTALL_FOLDER` to the folder where the EPEC folder is located. For instance, if you have EPEC installed in your home folder, e.g. `/Users/yourusername/epec-vanilla/`, you will want to use a text editor to add the line

```bash
 export EPEC_INSTALL_FOLDER="/Users/yourusername/epec-vanilla/"
```

at the end of your `.bash_profile`, `.bashrc` or `.zshrc` file according to which shell you use (IMPORTANT: make sure you have the forward slash at the end of the path string!) and then source it via `source ~/.bash_profile`, `source ~/.bashrc` or `source ~/.zshrc`.

Finally, run `chmod +x /Users/yourusername/EPEC/epec` to make EPEC executable. Optionally, you can add the installation directory to your system's `PATH` variable.

## Usage

To let EPEC evaluate a specific domain, simply `cd` to the installation directory and then run `./epec PATH_TO_FILE`. For instance, if we wish to evaluate the Biggs example in `Examples/biggs.lp` simply write `./epec Examples/biggs.lp`. You should get an output similar to the following:

```prolog
+-============================-+
|      EPEC Running...         |
+-============================-+
Running on: Examples/biggs.lp 

Trying n. traces: 1
Trying n. traces: 2
Trying n. traces: 3
Trying n. traces: 4
Trying n. traces: 5
Trying n. traces: 6
Trying n. traces: 7
Trying n. traces: 8
Trying n. traces: 9
Trying n. traces: 10
Trying n. traces: 11
Trying n. traces: 12
holds(1,((moneyInBag,false),-2)) ... % Full answer set not shown
+-============================-+
| Program over. N. traces: 12
+-============================-+
```

## Usage with the web GUI

If you wish to use EPEC with the web GUI available at [EPEC to ASP Translator](https://www.ucl.ac.uk/infostudies/epec/translator.html) you should follow these steps:

1. Write your domain description

2. Click on "Save ASP to File" and choose a suitable filename for your domain. The ASP file will typically be saved in your "Downloads" folder

3. Assume that the file was saved in `INPUT_PATH/domain.lp`. Then, `cd` to the EPEC directory and run `./epec -q ~/Downloads/domain.lp > OUTPUT_PATH/domain.eas` where `OUTPUT_PATH` is the path you wish to save your output file to

4. In the "EPEC Query" section of the GUI, select "Choose File" and select the output file

5. You are now ready to query the domain description!

## Changelog

- **v.0.1** (1 mar 2022): A javascript interface is now available!
- **v.0.0.9** (18 jun 2019): Major bugfixes.
- **v.0.0.8** (30 may 2019): EPEC now  closely matches the "original" semantics
- **v.0.0.7** (1 may 2019): a crucial bug which prevented EPEC from dealing with conditional o-props was corrected
- **v.0.0.6** (1 may 2019): heuristic mode was removed and can no longer be used. It is now possible to visualise traces using the `--show` option. Try e.g. `python epec.py Examples/medical_domain.lp --show 1`
- **v.0.0.5** (28 apr 2019): the way traces are numbered was significantly improved - can now deal with much bigger domains
- **v.0.0.4** (24 apr 2019): heuristic mode was added, can be used by specifying the `—heuristic-mode` flag
- **v.0.0.3** (24 apr 2019): first sound-complete working alpha
